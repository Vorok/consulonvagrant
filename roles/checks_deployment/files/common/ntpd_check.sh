#!/bin/sh
# 
ntpd=$(ps aux | grep  /usr/sbin/ntpd | grep -v grep | wc -l)

# Check the process number
# exit code 0 (ok), 1 (warning), 2 (error)
if [ $ntpd -eq 1 ]
then
  echo "OK: Process /usr/sbin/ntpd is active"
  exit 0
else
  echo "ERROR: the process /usr/sbin/ntpd is" $ntpd "times"
  exit 2
fi
